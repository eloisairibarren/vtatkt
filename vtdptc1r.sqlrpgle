**free
ctl-opt main(main);

dcl-f vtdptc1w workstn sfile(s01:nrr) alias usropn;

dcl-s nrr packed(4:0) inz(*zero);

dcl-ds C1 extname('VTDP') alias end-ds;

dcl-proc main;

    dcl-s srcstr varchar(52);

    exsr inicializar_programa;

    exfmt P01;
    dow *in03 =  *off;
        exsr mostrar_result_set;        
        exfmt P01;
    enddo;

    exsr finalizar_programa;

    //-------------------------------------------------
    begsr inicializar_programa;
    //-------------------------------------------------
        open vtdptc1w;
    endsr;
    
    //-------------------------------------------------
    begsr finalizar_programa;
    //-------------------------------------------------
        close vtdptc1w;     
        return;
    endsr;
    
    //-------------------------------------------------
   // begsr trabajar_con_result_set;
    //-------------------------------------------------
      //  exsr mostrar_result_set;
    //    dow *in12 = *off;
     //       exsr trabajar_con_selecciones;
     //       exsr mostrar_result_set;
     //   enddo;
 //   endsr;
    //-------------------------------------------------
   // begsr trabajar_con_selecciones;
    //-------------------------------------------------
      //  if *in06;
       //    npmptm0r('A':*zero);
            //leavesr;
       // endif;

     //   readc s01;
     //   dow not %eof();
       //     npmptm0r(opc:NRO_DOCUMENTO);
       //     readc s01;
     //   enddo;

   // endsr;
//


    //-------------------------------------------------
    begsr mostrar_result_set;
    //-------------------------------------------------

        *in30 = *off;
        *in31 = *off;
        *in40 = *off;
        nrr = *zero;

        write c01;

        exsr abrir_cursor_lineal;
        exsr fetch_next;
        dow sqlcode = *zero and nrr < 99;
            nrr = nrr + 1;
            write s01;
            exsr fetch_next;
        enddo;     
        exsr cerrar_cursor_lineal;

        if nrr >= 99;
            *in40 = *on;
        endif;

        *in31=*on;
        if nrr > 0;
           *in30 = *on;
        endif;

        write f01;
        exfmt c01;

    endsr;

    //-------------------------------------------------
    begsr abrir_cursor_lineal;
    //-------------------------------------------------
        
        srcstr = '%'+%trim(SRCNME)+'%';

        exec sql DECLARE C1 CURSOR FOR
            SELECT
                *
            FROM VTDP
            WHERE 
                    NOMBRE_EMPLEADO LIKE :SRCSTR
            OR      CAST(NUMERO_DNI AS CHAR (8)) LIKE :SRCSTR
        ;

        exec sql OPEN C1;

    endsr;

    //-------------------------------------------------
    begsr fetch_next;
    //-------------------------------------------------
        
        exec sql FETCH C1 INTO :C1;

    endsr;

    //-------------------------------------------------
    begsr cerrar_cursor_lineal;
    //-------------------------------------------------
        exec sql CLOSE C1;
    endsr;

end-proc;


