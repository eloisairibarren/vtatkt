**free

// Este es un programa de ejemplo que genera registro de personas al azar
// Pueden usarlo como base para generar una base de empleados para el 
// sistema de venta de tickets, habrá , claro, que cambiar los campos
// y el nombre de la tabla pero estos cambios serán mínimos 
 
 
ctl-opt main(main)  DFTACTGRP(*NO);

dcl-proc main;

    dcl-s NUMERO_TICKET    packed(5:0);
    dcl-s TIPO_TICKET   varchar(15);
    dcl-s PRECIO_TICKET   packed(5:0);
    dcl-s DESCRIPCION_TICKET varchar(50 );

    dcl-s i packed(8:0);

    exsr limpiar_tabla;

// entradas vip 
    for i = 1 to 300;
        NUMERO_TICKET   = generar_nro_ticket();
        TIPO_TICKET = 'VIP';    
        PRECIO_TICKET = 5000;
        DESCRIPCION_TICKET = 'INGRESAR POR ACCESO NORTE';
        exsr insertar_registro_vip;
    endfor;

    return;

//entradas palco 
    for i = 1 to 200;
        NUMERO_TICKET = generar_nro_ticket();
        TIPO_TICKET = 'PALCO';    
        PRECIO_TICKET = 7500;
        DESCRIPCION_TICKET = 'INGRESAR POR ACCESO ESTE';
        exsr insertar_registro_palco;
    endfor;

    return ;

//entradas rancho 
    for i = 1 to 1000;
        NUMERO_TICKET = generar_nro_ticket();
        TIPO_TICKET = 'RANCHO';    
        PRECIO_TICKET = 3000;
        DESCRIPCION_TICKET = 'INGRESAR POR ACCESO SUR';
        exsr insertar_registro_rancho;
    endfor;

    return;
    
    //---------------------------------------------------------
    begsr limpiar_tabla;
    //---------------------------------------------------------
        exec sql DELETE FROM VT5848D.VTTK;
    endsr;

    //---------------------------------------------------------
    begsr insertar_registro_palco;
    //---------------------------------------------------------
            exec sql INSERT INTO VT5848D/VTTK (
                NUMERO_TICKET, 
                TIPO_TICKET,       
                PRECIO_TICKET, 
                DESCRIPCION_TICKET 
                ) 
                VALUES
                (   
                      :NUMERO_TICKET
                    , :TIPO_TICKET 
                    , :PRECIO_TICKET
                    , :DESCRIPCION_TICKET
                    
                );                         
    endsr;


 //---------------------------------------------------------
    begsr insertar_registro_vip;
    //---------------------------------------------------------
            exec sql INSERT INTO VT5848D/VTTK (
                NUMERO_TICKET, 
                TIPO_TICKET,       
                PRECIO_TICKET, 
                DESCRIPCION_TICKET 
                ) 
                VALUES
                (   
                      :NUMERO_TICKET
                    , :TIPO_TICKET 
                    , :PRECIO_TICKET 
                    , :DESCRIPCION_TICKET
                    
                );                         
    endsr; 


 //---------------------------------------------------------
    begsr insertar_registro_rancho;
    //---------------------------------------------------------
            exec sql INSERT INTO VT5848D/VTTK (
                NUMERO_TICKET, 
                TIPO_TICKET,       
                PRECIO_TICKET, 
                DESCRIPCION_TICKET 
                ) 
                VALUES
                (   
                      :NUMERO_TICKET
                    , :TIPO_TICKET 
                    , :PRECIO_TICKET
                    , :DESCRIPCION_TICKET
                    
                );                         
    endsr;   

end-proc;


//==========================================================================
dcl-proc generar_nro_ticket;
//==========================================================================
    dcl-pi *n packed(5:0);
    end-pi;

    dcl-s nrotkt packed(5:0);
    dcl-s existe packed(5:0);

        dow *on;
            exec sql    SELECT 
                      INT((RANDOM()*99998)+1)  INTO :nrotkt
                    FROM sysibm.sysdummy1;
        
            exec sql    SELECT  
                            COUNT(*) INTO :existe
                        FROM VT5848D.VTTK
                        WHERE
                            NUMERO_TICKET = : nrotkt;
            if existe = 0 ;
                leave;
            endif;
        enddo;

        return nrotkt;
end-proc;
//==========================================================================
