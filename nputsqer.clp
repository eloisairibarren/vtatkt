pgm (&sqlcode &msgtxt)

    dcl &sqlcode *dec (4 0)
    dcl &msgcod  *char (4)
    dcl &msgid   *char (7)
    dcl &msgtxt  *char (50)

    chgvar &sqlcode (&sqlcode * -1)
    chgvar &msgcod  &sqlcode


    chgvar &msgid ('SQL'|| &msgcod)

    RTVMSG MSGID(&MSGID) +   
       MSGF(QSQLMSG )    +
       MSG(&MSGTXT)    

endpgm
