CREATE TABLE VT5848D.VTCU (

  ID_CUOTA        DEC     (8, 0)   NOT NULL WITH DEFAULT,
  NUMERO_DNI		  DEC     (8, 0)   NOT NULL WITH DEFAULT, 
  NUMERO_CUOTA 		  DEC     (1, 0)  NOT NULL WITH DEFAULT, 
  IMPORTE_CUOTA     DEC     (5, 0)   NOT NULL WITH DEFAULT,

  PRIMARY KEY (ID_CUOTA)
);    

LABEL ON TABLE VT5848D.VTCU IS                    
'VENTA DE TICKETS - CUOTA';                                                
