**free

// Este es un programa de ejemplo que genera registro de personas al azar
// Pueden usarlo como base para generar una base de empleados para el 
// sistema de venta de tickets, habrá , claro, que cambiar los campos
// y el nombre de la tabla pero estos cambios serán mínimos 
 
 
ctl-opt main(main)  DFTACTGRP(*NO);

dcl-s apellidos   varchar(20)  dim(10) ctdata perrcd(1);
dcl-s nombres     varchar(20)  dim(10) ctdata perrcd(1);

dcl-proc main;

    dcl-s NUMERO_DNI     packed(8:0);
    dcl-s NOMBRE_EMPLEADO   varchar(50);
    dcl-s SUELDO_EMPLEADO   packed(15:2);
    dcl-s LIMITEDISP_EMPLEADO packed(5:0);
    dcl-s LIMITEMAX_EMPLEADO packed(5:0);

    dcl-s i  packed(8:0);

    exsr limpiar_tabla;

    for i = 1 to 500;
        NUMERO_DNI   = generar_nro_documento();
        LIMITEDISP_EMPLEADO = 20000;
        LIMITEMAX_EMPLEADO = 20000;
        NOMBRE_EMPLEADO = generar_nombre();    
        SUELDO_EMPLEADO = generar_nro_aleatorios(1:999) *100 +100000;
        exsr insertar_registro;
    endfor;
    

    return ;
    
    //---------------------------------------------------------
    begsr limpiar_tabla;
    //---------------------------------------------------------
        exec sql DELETE FROM VT5848D.VTDP;
    endsr;

    //---------------------------------------------------------
    begsr insertar_registro;
    //---------------------------------------------------------
            exec sql INSERT INTO VT5848D/VTDP (
                NUMERO_DNI, 
                NOMBRE_EMPLEADO,       
                LIMITEMAX_EMPLEADO, 
                LIMITEDISP_EMPLEADO, 
                SUELDO_EMPLEADO 
                ) 
                VALUES
                (   
                      :NUMERO_DNI
                    , :NOMBRE_EMPLEADO 
                    , :LIMITEMAX_EMPLEADO
                    , :LIMITEDISP_EMPLEADO
                    , :SUELDO_EMPLEADO
                    
                );                         
    endsr;

end-proc;

//==========================================================================
//dcl-proc generar_nro_legajo;
//==========================================================================
   // dcl-pi *n packed(8:0);
   // end-pi;

    //dcl-s nroleg packed(8:0);

        //exec sql    SELECT 
                  //  INT((RANDOM()*99998)+1)  INTO :nroleg
               // FROM sysibm.sysdummy1;
        
        //return nroleg;
//end-proc;


//==========================================================================
dcl-proc generar_nro_documento;
//==========================================================================
    dcl-pi *n packed(8:0);
    end-pi;

    dcl-s nrodoc packed(8:0);
    dcl-s existe packed(8:0);

        dow *on;
            exec sql    SELECT 
                      INT((RANDOM()*900000)+20000000)  INTO :nrodoc
                    FROM sysibm.sysdummy1;
        
            exec sql    SELECT  
                            COUNT(*) INTO :existe
                        FROM VT5848D/VTDP
                        WHERE
                            NUMERO_DNI = : nrodoc;
            if existe = 0 ;
                leave;
            endif;
        enddo;

        return nrodoc;
end-proc;
//==========================================================================
dcl-proc generar_nombre;
//==========================================================================
    dcl-pi *n varchar(50);
    end-pi;

    dcl-s j                 packed(2:0);
    dcl-s primer_nombre     varchar(20);
    dcl-s segundo_nombre    varchar(20);
    dcl-s primer_apellido          varchar(20);

    dcl-s nombre_completo   varchar(50);

    exsr generar_index;
    primer_nombre = nombres(j);

    exsr generar_index;
    segundo_nombre = nombres(j);

    if primer_nombre = segundo_nombre;
        exsr generar_index;
        segundo_nombre = nombres(j);
    endif;

    exsr generar_index;
    primer_apellido = apellidos(j);

    nombre_completo =   primer_apellido
                        +' ' +primer_nombre
                        +' ' + segundo_nombre;                    

    return nombre_completo;

    //---------------------------------------------------------
    begsr generar_index;
    //---------------------------------------------------------
        exec sql    SELECT 
                    INT((RANDOM()*9)+1)  INTO :j
                FROM sysibm.sysdummy1;
    endsr;

    

end-proc;

//==========================================================================
dcl-proc generar_nro_aleatorios;
//==========================================================================
        dcl-pi *n       packed(15:0);
            min         packed(4:0) const;        
            max         packed(4:0) const;        
        end-pi;

        dcl-s rndnbr    packed(15:0);

        exec sql    SELECT 
                        INT((RANDOM()*:max)+:min)  INTO :rndnbr
                    FROM sysibm.sysdummy1;

        return  rndnbr;
end-proc;


//==========================================================================
//dcl-proc generar_fecha;
//==========================================================================
  //  dcl-pi *n date;
    //    year        packed(4:0) const;        
     //   offset      packed(4:0) const;        
  //  end-pi;

    
  //  dcl-s anio         packed(4:0);
  //  dcl-s mes          packed(2:0);
  //  dcl-s dia          packed(2:0);
  //  dcl-s fecha_char   char(10);

 //   dcl-s nueva_fecha   date;

  //  exec sql    SELECT 
    //                INT((RANDOM()*11)+1)  INTO :mes
        //        FROM sysibm.sysdummy1;

   // exec sql    SELECT 
      //              INT((RANDOM()*27)+1)  INTO :dia
          //      FROM sysibm.sysdummy1;
    //exec sql    SELECT 
                  //  INT((RANDOM()* :offset )+1)  INTO :anio
              //  FROM sysibm.sysdummy1;                

   // anio = anio + year;
   // fecha_char = %char(anio) 
        //        + '-'
             //   + %trim(%editw( mes: '0  '))
              //  + '-'
             //   + %trim(%editw( dia: '0  '));

   // nueva_fecha = %date(fecha_char:*ISO);

    //return nueva_fecha;
//end-proc;    
**
ROMERO
GOMEZ
PEREZ
DIEGUEZ
LOPEZ
IRIBARREN
MARTINEZ
LOPEZ
ALE 
RODRIGUEZ
**
HUGO
SANTIAGO
LUIS
MARTIN
DANIEL
FRANCISCO
MATIAS
FEDERICO
RAMON
LUCIANO